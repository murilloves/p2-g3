﻿# APLICAÇÕES DISTRIBUÍDAS P2-G3 - PROJETO 2 WEBSOCKET

## MEMBROS DO PROJETO

* Tiago Moreira Vaz - Líder @hogait
* Rhandy Mendes Ferreira
* Paulo Henrique Abreu Neiva - Documentação


## DEPENDENCIAS IRC-WEBSOCKET
* Instalar todas as bibliotecas solicitadas através do comando:
**npm install nomeDaBiblioteca**

* Bibliotecas que deverá ser instaladas até o momento:
**express**, **body-parser**, **cookie-parser**, **socket.io**, **socket.io-cookie**, **irc**


## TUTORIAL NODE.js
* Acessar local do arquivo **app.js** através da linha de comando: 
**prompt/terminal**

* Para executar o servidor **app.js** utilizar a seguinte linha de comando: 
**node app.js**

* Para finalizar a execução: 
**Ctrl + c**


## TUTORIAL IRC
* Abrir browser e acessar: **localhost:8080**